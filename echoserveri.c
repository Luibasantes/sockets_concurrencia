#include "csapp.h"

void echo(int connfd);

void sigchld_handler(int sig)
{
while (waitpid(-1, 0, WNOHANG) > 0);
return;
}

int main(int argc, char **argv)
{
	int listenfd, connfd;
	unsigned int clientlen;
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	char *haddrp, *port;

	if (argc != 2) {
		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(0);
	}
	
	port = argv[1];
	Signal(SIGCHLD, sigchld_handler);
	listenfd = Open_listenfd(port);
	while (1) {
		clientlen = sizeof(clientaddr);
		connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);

		/* Determine the domain name and IP address of the client */
		hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
					sizeof(clientaddr.sin_addr.s_addr), AF_INET);
		haddrp = inet_ntoa(clientaddr.sin_addr);
		printf("server connected to %s (%s)\n", hp->h_name, haddrp);

		if (Fork() == 0) {
			Close(listenfd); /* Child closes its listening socket */
			echo(connfd);
			/* Child services client */
			Close(connfd);
			/* Child closes connection with client */
			exit(0);
			/* Child exits */
		}
		Close(connfd);
	}
}

void echo(int connfd)
{
	size_t n;
	char buf[MAXLINE];
	char *const argv[MAXLINE];
	rio_t rio;
	pid_t pid;
	int status;


	Rio_readinitb(&rio, connfd);
	while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0) {

		//buf[strcspn(buf, "\n")] = 0;

		parseline(buf,argv);

		printf("server received %lu bytes, received this command: %s\n", n,buf);
		if ((pid=fork())==0){
			if(execve(argv[0],argv,environ)<0){
				raise(SIGABRT);
				printf("%s: Commando Erroneo.\n",buf);
				Rio_writen(connfd, "ERROR\n", strlen("ERROR\n"));
				exit(1);
			}
		}else{

			waitpid(pid,&status,0);
			if (status!=0){
				Rio_writen(connfd, "ERROR\n", strlen("ERROR\n"));
			}
			else{
				Rio_writen(connfd, "OK\n", strlen("OK\n"));
			}

		}	
	}
}



int parseline(char *buf, char **argv)
{
	char *delim;
	int argc;
	int bg;


	buf[strlen(buf)-1] = ' '; /* Replace trailing ’\n’ with space */
	while (*buf && (*buf == ' ')) /* Ignore leading spaces */
		buf++;

/* Build the argv list */
	argc = 0;
	while ((delim = strchr(buf, ' '))) {
		argv[argc++] = buf;
		*delim = '\0';
		buf = delim + 1;
		while (*buf && (*buf == ' ')) /* Ignore spaces */
			buf++;
	}
	argv[argc] = NULL;

	if (argc == 0)
		return 1;


/* Should the job run in the background? */
	if ((bg = (*argv[argc-1] == '&')) != 0)
		argv[--argc] = NULL;

	return bg;

}
